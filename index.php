
<?php
    session_start();
    if (!empty($_SESSION['status'])) {
        header("location: http://localhost/disposisi-surat/surat.php");
    }
?>


<!doctype html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>Disposisi Surat Bagian ALHP</title>
        <link href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' rel='stylesheet'>
        <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet'>
        <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<style>

body {
    color: #000;
    overflow-x: hidden;
    height: 100%;
    background-color: #B0BEC5;
    background-repeat: no-repeat
}

.card0 {
    box-shadow: 0px 4px 8px 0px #757575;
    border-radius: 0px
}

.card2 {
    margin: 120px 40px
}

.logo {
    width: 100px;
    height: 100px;
    margin-top: 20px;
    margin-left: 35px
}

.judul {
    margin-top: 40px;
    margin-left: 20px;
}

.image {
    width: 360px;
    height: 280px
}

.border-line {
    border-right: 1px solid #EEEEEE
}


.line {
    height: 1px;
    width: 45%;
    background-color: #E0E0E0;
    margin-top: 10px
}

.or {
    width: 10%;
    font-weight: bold
}

.text-sm {
    font-size: 14px !important
}

::placeholder {
    color: #BDBDBD;
    opacity: 1;
    font-weight: 300
}

:-ms-input-placeholder {
    color: #BDBDBD;
    font-weight: 300
}

::-ms-input-placeholder {
    color: #BDBDBD;
    font-weight: 300
}

input,
textarea {
    padding: 10px 12px 10px 12px;
    border: 1px solid lightgrey;
    border-radius: 2px;
    margin-bottom: 5px;
    margin-top: 2px;
    width: 100%;
    box-sizing: border-box;
    color: #1f898d;
    font-size: 14px;
    letter-spacing: 1px
}

input:focus,
textarea:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: 1px solid #0d7a25;
    outline-width: 0
}

button:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    outline-width: 0
}

a {
    color: inherit;
    cursor: pointer
}

.btn-blue {
    background-color: #15b182;
    width: 150px;
    color: #fff;
    border-radius: 2px
}

.btn-blue:hover {
    background-color: rgb(29, 117, 51);
    cursor: pointer
}

.bg-blue {
    color: #fff;
    background-color: #15b182
}

@media screen and (max-width: 991px) {
    .logo {
        margin-left: 0px
    }

    .image {
        width: 300px;
        height: 220px
    }

    .border-line {
        border-right: none
    }

    .card2 {
        border-top: 1px solid #EEEEEE !important;
        margin: 0px 15px
    }
}</style>
                                
</head>
    <body oncontextmenu='return false' class='snippet-body'>
    <div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
    <div class="card card0 border-0">
        <div class="row d-flex">
            <div class="col-lg-6">
                <div class="card1 pb-5">
                    <div class="row"> 
                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/63/Lambang_Kementerian_Lingkungan_Hidup_dan_Kehutanan.png/600px-Lambang_Kementerian_Lingkungan_Hidup_dan_Kehutanan.png" class="logo"> 
                        <h1 class="judul">DISPOSISI SURAT ALHP</h1>
                    </div>
                    <div class="row px-3 justify-content-center mt-4 mb-5 border-line"> <img src="https://i.imgur.com/uNGdWHi.png" class="image"> </div>
                </div>
            </div>
            <div class="col-lg-6">
                <form action="http://localhost/disposisi-surat/c_login.php" method="POST">
                    <div class="card2 card border-0 px-4 py-5">
                        <div class="row px-3"> <label class="mb-1">
                            <h6 class="mb-0 text-sm">NIP</h6>
                            </label> <input class="mb-4" type="text" name="username" placeholder="Masukkan NIP"> 
                        </div>
                        <div class="row px-3"> <label class="mb-1">
                            <h6 class="mb-0 text-sm">Password</h6>
                            </label> <input type="password" name="password" placeholder="Masukkan password"> 
                        </div>
                        
                        <?php 
                            if (!empty($_SESSION['errorMessage'])) {
                                echo '<div class="row px-3 mb-4" style="color:red">NIP atau Password salah</div>'; // show it to the user
                                unset($_SESSION['errorMessage']); // so as not to display it every time
                            } else {
                                echo '<div class="row px-3 mb-4"></div>';
                            }
                        ?>
                
                        <div class="row mb-3 px-3"> <button type="submit" class="btn btn-blue text-center">Login</button> </div>
                    </div>
                </form>
            </div>
        </div>
        
        <div class="bg-blue py-4">
            <div class="row px-3"> <small class="ml-4 ml-sm-5 mb-2">Copyright &copy; 2019. All rights reserved.</small></div>
        </div>
    </div>
</div>
<script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js'></script>
<script type='text/javascript'></script>


</body>
</html>