<?php
    session_start(); 
    // menghubungkan dengan koneksi
    include('db.php');

    $id = $_GET['id'];

    //get all surat
    $data_surat = mysqli_query($connection,"select * from surat where id_surat='$id'");
    $row_surat = mysqli_fetch_assoc($data_surat);

    //get all data user
    $data_user = mysqli_query($connection,"select * from user where level=2 OR level=3");

    //get data pengolah
    $sql_pengolah = "SELECT * FROM user INNER JOIN pengolah ON user.id_user = pengolah.id_penerima 
                    WHERE pengolah.id_surat='$id'";
    $data_pengolah = mysqli_query($connection, $sql_pengolah);

    //get detail disposisi
    $data_disposisi = mysqli_query($connection, "select * from disposisi where id_surat_masuk='$id'");

    if (mysqli_num_rows($data_disposisi) == 1){
        $detail_disposisi = mysqli_fetch_assoc($data_disposisi);
        $_SESSION['disposisiId'] = $detail_disposisi['id_disposisi'];
    }else{
        if (!empty($_SESSION['disposisiId'])) {
            unset($_SESSION['disposisiId']);
        }
    }
?>

<!doctype html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>Disposisi Surat Bagian ALHP</title>
        <link href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' rel='stylesheet'>
        <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet'>
        <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>                       
    </head>

    <style>
        .body-all{
            margin-bottom: 20px;
        }

        .bg-blue {
            color: #fff;
            width: 100%;
            top: 0px;
            z-index: 2;
            background-color: #15b182;
            position: fixed;
            padding: 20px 50px 20px 50px;
        }

        .btn-arsip {
            float: right;
        }

        .btn-diposisi {
            float: right;
            margin-right: 10px;
        }

        #tb-surat {
            margin-top: 50px;
        }

        .top-button{
            margin-top: 100px;
        }

        .borderless td, .borderless th {
            border: none;
            padding: 0px;
        }

        .borderless{
            margin-top: 20px;
        }

        .catatan-area{
            height: 150px;
            background-color: azure;
            padding: 10px;
        }

        .tgl-disposisi p {
            float: right;
        }

        .pdf-viewer{
            width: 100%;
            margin-top: 20px;
        }

        .label a, .logout a {
            color: inherit; /* blue colors for links too */
            text-decoration: inherit; /* no underline */
        }

        .label a:hover, .logout a:hover {
            color: white;
        }

        .logout {
            display: flex; 
            justify-content: flex-end;
        }
    </style>
    
    <body oncontextmenu='return false' class='body-all'>
        <div class="bg-blue row">
            <div class="label col"><h5><a href="http://localhost/disposisi-surat/index.php">DISPOSISI SURAT BAGIAN ALHP</a></h5></div>
            <div class="logout col">
                <a href="http://localhost/disposisi-surat/c_logout.php">Logout</a>
            </div>
        </div>

        <div class="container">
            <div class="row top-button">
                <div class="col-sm"></div>
                
                <?php
                    if($_SESSION['level'] == 1 || $_SESSION['level'] == 2 ) {
                        // echo '<div class="col-sm button-top">
                        //     <button type="button" class="btn btn-danger btn-arsip" data-toggle="modal" data-target="#arsipDialog">Arsipkan Surat</button>
                        //     <button type="button" class="btn btn-success btn-diposisi" data-toggle="modal" data-target="#disposisiDialog">Disposisi Surat</button>
                        // </div>';
                        if(!($_SESSION['level'] == 2 && empty($detail_disposisi['tanggal_disposisi_kabag']))){
                            echo '<div class="col-sm button-top">
                                <button type="button" class="btn btn-success btn-diposisi" data-toggle="modal" data-target="#disposisiDialog">Disposisi Surat</button>
                            </div>';
                        }
                    }
                ?> 
            </div>
                
            <table class='table borderless'>
                <tbody>
                    <tr>
                        <td style="width: 10%"><p class="font-weight-light">Sifat Surat</p></td>
                        <td style="width: 2%">:</td>
                        <td><p class="font-weight-light"><?php echo ucfirst($row_surat['sifat_surat'])?></p></td>
                    </tr>
                    <tr>
                        <td style="width: 10%"><p class="font-weight-light">No Surat</p></td>
                        <td style="width: 2%">:</td>
                        <td><p class="font-weight-light"><?php echo $row_surat['no_surat']?></p></td>
                    </tr>

                    <tr>
                        <td style="width: 10%"><p class="font-weight-light">Asal Surat</p></td>
                        <td style="width: 2%">:</td>
                        <td><p class="font-weight-light"><?php echo $row_surat['asal_surat']?></p></td>
                    </tr>

                    <tr>
                        <td style="width: 10%"><p class="font-weight-light">Isi / Perihal</p></td>
                        <td style="width: 2%">:</td>
                        <td><p class="font-weight-light"><?php echo $row_surat['perihal_surat']?></p></td>
                    </tr>

                    <tr>
                        <td style="width: 10%"><p class="font-weight-light">Lapiran</p></td>
                        <td style="width: 2%">:</td>
                        <td><p class="font-weight-light "><?php echo $row_surat['lampiran_surat']?></p></td>
                    </tr>

                    <tr>
                        <td style="width: 10%"><p class="font-weight-light">Catatan</p></td>
                        <td style="width: 2%">:</td>
                        <td><p class="font-weight-light "><?php echo $row_surat['catatan_surat']?></p></td>
                    </tr>
                </tbody>
            </table>

            <div>
                <p>Kasubag Pengolah</p>  
                 <p class="font-weight-light">
                    <?php
                        $fnd = 0;
                        foreach($data_pengolah as $row) {
                            if($row['level'] == 2){
                                echo $row['nama'];
                                echo "<br />";
                                $fnd++;
                            }
                        }

                        if ($fnd == 0){
                            echo "-";
                        }

                    ?>
                 </p>
                 <p>Staf Pengolah</p>  
                 <p class="font-weight-light">
                    <?php
                       $fnd = 0;
                       foreach($data_pengolah as $row) {
                           if($row['level'] == 3){
                               echo $row['nama'];
                               echo "<br />";
                               $fnd++;
                           }
                       }

                       if ($fnd == 0){
                           echo "-";
                       }
                    ?>
                 </p>
                 <p>Isi Disposisi</p>  
                 <p class="font-weight-light">
                    <?php
                    if(!empty($detail_disposisi['isi_disposisi'])){
                        $tempDisp = $detail_disposisi['isi_disposisi'];
                        $disp = explode(',', $tempDisp);

                        foreach($disp as $value){
                            switch ($value) {
                                case 1:
                                  echo "Untuk diketahui";
                                  break;
                                case 2:
                                    echo "Untuk diselesaikan";
                                  break;
                                case 3:
                                    echo "Untuk dibuatkan resume";
                                  break;
                                case 4:
                                    echo "Minta penjelasan";
                                    break;
                                case 5:
                                    echo "Ikuti perkembangan";
                                    break;  
                                default:
                                    echo "-";
                            } 
                            echo "<br />";
                        }
                    }else{
                        echo "-";
                    }
                        
                    ?>
                 </p>
            </div>

            <div class="row">
                <div class="col-sm">
                    <div class="row">
                        <div class="col-sm">
                            <p>Catatan Kabag</p>
                        </div>
                        <div class="col-sm tgl-disposisi">
                            <p>
                                <?php

                                    if(!empty($detail_disposisi['tanggal_disposisi_kabag'])){
                                        $tgl = $detail_disposisi['tanggal_disposisi_kabag'];
                                        if($tgl != "0000-00-00")
                                            echo date("d/m/Y", strtotime($tgl));
                                    }
                                    
                                ?>
                            </p>
                        </div>

                    </div>
                    <div class="catatan-area"><?php 
                    
                        if(!empty($detail_disposisi['catatan_kabag'])){
                            echo $detail_disposisi['catatan_kabag'];
                        }else{
                            echo "-";
                        }
                       
                    ?>
                    
                </div>
                </div>
                <div class="col-sm">
                    <div class="row">
                        <div class="col-sm">
                            <p>Catatan Kasubag</p>
                        </div>
                        <div class="col-sm tgl-disposisi">
                            <p>
                                <?php
                                    if(!empty($detail_disposisi['tanggal_disposisi_kasubag'])){
                                        $tgl = $detail_disposisi['tanggal_disposisi_kasubag'];
                                        if($tgl != "0000-00-00")
                                            echo date("d/m/Y", strtotime($tgl));
                                    }
                                ?>
                            </p>
                        </div>

                   </div>
                        <div class="catatan-area"> <div class="catatan-area">
                            <?php 
                                if(!empty($detail_disposisi['catatan_kasubag'])){
                                    echo $detail_disposisi['catatan_kasubag'];
                                }else{
                                    echo "-";
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <iframe class="pdf-viewer" src="<?php echo $row_surat['link_pdf']?>" width="640" height="480" allow="autoplay"></iframe>

        </div>

        <!-- Modal -->
    
        <div class="modal fade" id="arsipDialog" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Yakin arsipkan</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Apakah anda yakin surat ini telah selesai dilaksanakan?</p>
                </div>
                <div class="modal-footer">
                    <a href="http://localhost/disposisi-surat/c_arsipsurat.php?id=<?php echo $id?>"><button type="button" class="btn btn-primary">Yes</button></a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
        </div>

        <div class="modal fade" id="disposisiDialog" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Disposisi</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form action="http://localhost/disposisi-surat/c_disposisisurat.php?id=<?php echo $id ?>" method="POST">
                    <div class="modal-body">
                        <p class="font-weight-bold">Pengolah</p>
                    
                        <div style="margin-bottom: 20px;">

                            <?php
                                if (mysqli_num_rows($data_user) > 0) {
                                    // output data of each row
                                    $i = 1;

                                   
                                    while($row = mysqli_fetch_assoc($data_user)) {
                                        if(mysqli_num_rows($data_pengolah) > 0){
                                            $fnd = false;
                                            foreach($data_pengolah as $pengolah){
                                                if($row["id_user"] == $pengolah["id_penerima"]){
                                                    echo '<div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="pengolah[]" value="'.$row["id_user"].','.$row["level"].'" id="pengolah" checked>
                                                    <label class="form-check-label" for="pengolah">'.$row["nama"].'</label>
                                                    </div>';
                                                    $fnd = true;
                                                    break;
                                                }
                                            }

                                            if(!$fnd){
                                                echo '<div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="pengolah[]" value="'.$row["id_user"].','.$row["level"].'" id="pengolah">
                                                <label class="form-check-label" for="pengolah">'.$row["nama"].'</label>
                                                </div>';
                                            }
                                        }else{
                                            echo '<div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="pengolah[]" value="'.$row["id_user"].','.$row["level"].'" id="pengolah">
                                            <label class="form-check-label" for="pengolah">'.$row["nama"].'</label>
                                            </div>';
                                        }
                                        
                                    }
                                }
                            ?>
                        </div>
                    
                        <p class="font-weight-bold">Isi Disposisi</p>
                        <div style="margin-bottom: 20px;">
                            <?php
                                $isi_satu = "";
                                $isi_dua = "";
                                $isi_tiga = "";
                                $isi_empat = "";
                                $isi_lima = "";

                                if(!empty($detail_disposisi['isi_disposisi'])){
                                    if (str_contains($detail_disposisi['isi_disposisi'], ',')) {
                                        $isi = explode(",", $detail_disposisi['isi_disposisi']);
                                        foreach($isi as $val){
                                            switch($isi){
                                                case 1: $isi_satu = "checked"; break;
                                                case 2: $isi_dua = "checked"; break;
                                                case 3: $isi_tiga = "checked"; break;
                                                case 4: $isi_empat = "checked"; break;
                                                case 5: $isi_lima = "checked"; break;
                                            }
                                        }
                                    }else{
                                        switch($detail_disposisi['isi_disposisi']){
                                            case 1: $isi_satu = "checked"; break;
                                            case 2: $isi_dua = "checked"; break;
                                            case 3: $isi_tiga = "checked"; break;
                                            case 4: $isi_empat = "checked"; break;
                                            case 5: $isi_lima = "checked"; break;
                                        }
                                    }         
                                }
                            ?>
                            
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="1" name="disposisi[]" id="disposisi" <?php echo $isi_satu?>>
                                <label class="form-check-label" for="disposisi">Untuk diketahui</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="2" name="disposisi[]" id="disposisi" <?php echo $isi_dua?>>
                                <label class="form-check-label" for="disposisi">Untuk diselesaikan</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="3" name="disposisi[]" id="disposisi" <?php echo $isi_tiga?>>
                                <label class="form-check-label" for="disposisi">Untuk dibuatkan resume</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="4" name="disposisi[]" id="disposisi" <?php echo $isi_empat?>>
                                <label class="form-check-label" for="disposisi">Minta penjelasan</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="5" name="disposisi[]" id="disposisi" <?php echo $isi_lima?>>
                                <label class="form-check-label" for="disposisi">Ikuti perkembangan</label>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="catatanKabag">Catatan Kabag</label>
                            <textarea <?php if ($_SESSION['level'] != 1) echo "disabled"?> class="form-control" name="catatanKabag" id="catatanKabag" rows="3"><?php
                                    if(!empty($detail_disposisi['catatan_kabag'])){
                                        echo $detail_disposisi['catatan_kabag'];
                                    }
                                ?></textarea>
                        </div>

                        
                        <div class="form-group">
                            <label for="catatanKasubag">Catatan Kasubag</label>
                            <textarea <?php if ($_SESSION['level'] != 2) echo "disabled"?> class="form-control" rows="3" name="catatanKasubag" id="catatanKasubag"><?php
                                    if(!empty($detail_disposisi['catatan_kasubag'])){
                                        echo $detail_disposisi['catatan_kasubag'];
                                    }
                                ?></textarea>
                        </div>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
              </div>
            </div>
        </div>

          
        <script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js'></script>
        <script type='text/javascript'></script>
    </body>
</html>