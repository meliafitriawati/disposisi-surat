<?php

    session_start();
    // menghubungkan dengan koneksi
    include('db.php');

    $id = $_GET['id'];
    $level = $_SESSION['level'];
    $id_user = $_SESSION['id_user'];
    $pengolah = $_POST['pengolah'];
    $disposisi = implode (", ", $_POST['disposisi']);
    $now = date("Y-m-d H:i:s");
    // if login as kabag
    if($level == 1){
        //insert disposisi
        $catatanKabag = $_POST['catatanKabag'];

        if(!empty($_SESSION['disposisiId'])){
            $disposisiId = $_SESSION['disposisiId'];
            $sql = "UPDATE disposisi SET catatan_kabag='$catatanKabag', tanggal_disposisi_kabag='$now' WHERE id_disposisi='$disposisiId'";
        }else{
            $sql = "INSERT INTO disposisi(catatan_kabag, tanggal_disposisi_kabag, isi_disposisi, id_surat_masuk) VALUES 
            ('$catatanKabag','$now','$disposisi',$id)";
        }

        $resp = mysqli_query($connection, $sql);
        //insert pengolah
        if($resp){
            $sql = "DELETE FROM pengolah WHERE id_surat = '$id'";
            mysqli_query($connection, $sql);

            foreach($pengolah as $val){
                $array_pengolah = explode (",", $val);
                $penerimaId = $array_pengolah[0];
                $penerimaLevel = $array_pengolah[1];

                $sql = "INSERT INTO pengolah(id_surat, id_pengirim, level_pengirim, id_penerima, level_penerima) VALUES 
                        ('$id','$id_user','$level','$penerimaId','$penerimaLevel')"; 
                mysqli_query($connection, $sql);
            };

            header("location: http://localhost/disposisi-surat/detail_surat.php?id=$id");
            
        }
    } else if ($level == 2){
        //update data
        $catatanKasubag = $_POST['catatanKasubag'];

        $disposisiId = $_SESSION['disposisiId'];
        $sql = "UPDATE disposisi SET catatan_kasubag='$catatanKasubag', tanggal_disposisi_kasubag='$now' WHERE id_disposisi='$disposisiId'";

        $resp = mysqli_query($connection, $sql);

        if($resp){
            $sql = "DELETE FROM pengolah WHERE id_surat = '$id'";
            mysqli_query($connection, $sql);

            foreach($pengolah as $val){
                $array_pengolah = explode (",", $val);
                $penerimaId = $array_pengolah[0];
                $penerimaLevel = $array_pengolah[1];

                $sql = "INSERT INTO pengolah(id_surat, id_pengirim, level_pengirim, id_penerima, level_penerima) VALUES 
                        ('$id','$id_user','$level','$penerimaId','$penerimaLevel')"; 
                mysqli_query($connection, $sql);
            };

            header("location: http://localhost/disposisi-surat/detail_surat.php?id=$id");
        }
        
    }
   
?>