
<?php
    session_start(); 
?>


<!doctype html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>Disposisi Surat Bagian ALHP</title>
        <link href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' rel='stylesheet'>
        <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet'>
        <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>                       
    </head>

    <style>
        
        .bg-blue {
            color: #fff;
            background-color: #15b182;
            padding: 20px 50px 20px 50px;
        }

        .btn-add {
            float: right;
        }

        #tb-surat {
            margin-top: 50px;
        }

        .top-button{
            margin-top: 60px;
        }

        .logout {
            display: flex; 
            justify-content: flex-end;
        }

        .label a, .logout a, button a {
            color: inherit; /* blue colors for links too */
            text-decoration: inherit; /* no underline */
        }

        .label a:hover, .logout a:hover, button a:hover {
            color: white;
        }

    </style>
    
    <?php 
        if(isset($_GET['pesan'])){
            if($_GET['pesan'] == "gagal"){
                echo '<script type="text/javascript">',
                    'alert("data gagal ditambahkan")',
                    '</script>';
            } else {
                echo '<script type="text/javascript">',
                'alert("data berhasil ditambahkan")',
                '</script>';
            }
        }
	?>

    <body oncontextmenu='return false' class='snippet-body'>
        <div class="bg-blue row">
            <div class="label col"><h5><a href="http://localhost/disposisi-surat/index.php">DISPOSISI SURAT BAGIAN ALHP</a></h5></div>
            <div class="logout col">
                <a href="http://localhost/disposisi-surat/c_logout.php">Logout</a>
            </div>
        </div>

        <div class="container">
            <div class="row top-button">
              <div class="col-sm">
                <form action="" method="get">
                    <div class="input-group">
                        <input name="search" type="text" class="form-control rounded" placeholder="Search" aria-label="Pencarian Surat"
                        aria-describedby="search-addon" />
                        <?php ?>
                        <button name="cari" type="submit" class="btn btn-outline-success">Cari</button>
                    </div>
                </form>
              </div>
              <div class="col-sm">
                <?php
                    if($_SESSION['level'] == 4) {
                        echo '<button type="button" class="btn btn-outline-success btn-add" data-toggle="modal" data-target=".bd-tambah-surat">Tambah Surat</button>';
                    }
                ?> 
              </div>
            </div>

            <table id="tb-surat" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th style="width:3%">No</th>
                        <th style="width:20%">No. Surat</th>
                        <th style="width:18%">Asal Surat</th>
                        <th style="width:20%">Perilhal</th>
                        <th style="width:15%">Kasubag</th>
                        <th style="width:15%">Staf</th>
                        <th style="width:9%">Aksi</th>
                    </tr>

                    <?php
                        // menghubungkan dengan koneksi
                        include('db.php');

                        // getting all the data
                        if(isset($_GET['cari'])){
                            $search_keyword = $_GET['search'];
                            $data = mysqli_query($connection,"SELECT * FROM surat WHERE asal_surat LIKE '%".$search_keyword."%'");
                        }else{
                            $data = mysqli_query($connection,"SELECT * FROM surat");
                        }

                        

                        if (mysqli_num_rows($data) > 0) {
                            // output data of each row
                            $i = 1;
                            while($row = mysqli_fetch_assoc($data)) {
                                $id = $row['id_surat'];
                                //get data pengolah
                                //get data pengolah
                                $sql_pengolah = "SELECT * FROM user INNER JOIN pengolah ON user.id_user = pengolah.id_penerima 
                                WHERE pengolah.id_surat='$id'";
                                $data_pengolah = mysqli_query($connection, $sql_pengolah);

                                echo "<tr>";
                                echo "<td>".$i."</td>";
                                echo "<td>".$row["no_surat"]."</td>";
                                echo "<td>".$row["asal_surat"]."</td>";
                                echo "<td>".$row["perihal_surat"]."</td>";
                                if (mysqli_num_rows($data_pengolah) >= 1){
                                    $kasub_pengolah = "";
                                    $staf_pengolah = "";
                                    foreach($data_pengolah as $row) {
                                        if($row['level'] == 2){
                                           $kasub_pengolah = $kasub_pengolah . ' ' . $row['nickname'].'</br>';
                                        }else{
                                            $staf_pengolah = $staf_pengolah . ' ' . $row['nickname'].'</br>';
                                        }
                                    }

                                    echo "<td>".$kasub_pengolah."</td>";
                                    echo "<td>".$staf_pengolah."</td>";
                                }else{
                                    echo "<td>-</td>";
                                    echo "<td>-</td>";
                                }
                                echo "<td>
                                        <a href='http://localhost/disposisi-surat/detail_surat.php?id=$id'><span class='fa fa-eye mr-2'></span></a>
                                        <a href='http://localhost/disposisi-surat/c_deletesurat.php?id=$id'><span class='fa fa-trash'></span></a>
                                    </td>";
                                    // <a href='' id='$id' data-toggle='modal' data-target='.bd-update-surat'><span class='fa fa-pencil-square-o mr-2'></span></a>
                                $i++;
                            }
                        } else {
                            echo "0 results";
                        }
                    ?>
                </thead>
                <tbody>

                </tbody>
                
            </table>
          </div>

          <div class="modal fade bd-tambah-surat" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tambah Surat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="http://localhost/disposisi-surat/c_tambahsurat.php" method="POST">
                    <div class="modal-body">
                        <h6>Sifat Surat</h6>

                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sifat" id="kilat" value="kilat">
                            <label class="form-check-label" for="kilat">Kilat</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sifat" id="segera" value="segera">
                            <label class="form-check-label" for="segera">Segera</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sifat" id="rahasia" value="rahasia">
                            <label class="form-check-label" for="rahasia">Rahasia</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sifat" id="penting" value="penting">
                            <label class="form-check-label" for="penting">Penting</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sifat" id="biasa" value="biasa">
                            <label class="form-check-label" for="biasa">Biasa</label>
                        </div>

                        <form style="margin-top: 20px">
                            <div class="form-group">
                                <label for="asal_surat">Asal Surat</label>
                                <input type="text" class="form-control" id="asal_surat" name="asal_surat" placeholder="Asal Surat">
                            </div>

                            <div class="form-group">
                                <label for="no_surat">No Surat</label>
                                <input type="text" class="form-control" id="no_surat" name="no_surat" placeholder="No Surat">
                            </div>

                            <div class="form-group">
                                <label for="tanggal_surat">Tanggal Surat</label>
                                <input type="date" class="form-control" id="tanggal_surat" name="tanggal_surat" placeholder="Tanggal Surat">
                            </div>

                            <div class="form-group">
                                <label for="tanggal_diterima">Tanggal Diterima</label>
                                <input type="date" class="form-control" id="tanggal_diterima" name="tanggal_diterima" placeholder="Tanggal Diterima">
                            </div>

                            <div class="form-group">
                                <label for="isi_surat">Isi/Perihal</label>
                                <input type="text" class="form-control" id="isi_surat" name="isi_surat" placeholder="Isi/Perihal">
                            </div>

                            <div class="form-group">
                                <label for="lampiran">Lampiran</label>
                                <input type="text" class="form-control" id="lampiran" name="lampiran" placeholder="Lampiran">
                            </div>

                            <div class="form-group">
                                <label for="lampiran">Catatan</label>
                                <input type="text" class="form-control" id="catatan"name="catatan" placeholder="Catatan">
                            </div>
                
                            <div class="form-group">
                                <label for="inputPDF">Link PDF</label>
                                <input type="text" class="form-control" id="linkPDF" name="linkPDF" placeholder="Link PDF">
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </form>
                    </div>
                </form>    
              </div>
            </div>
          </div>
            
          <div id="update-surat" class="modal fade bd-update-surat" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Update Surat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
        
                <?php
                   echo $_POST['suratId'];
                ?>
                <form action="http://localhost/disposisi-surat/c_tambahsurat.php" method="POST">
                    <div class="modal-body">
                        
                        <h6>Sifat Surat</h6>

                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sifat" id="kilat" value="kilat">
                            <label class="form-check-label" for="kilat">Kilat</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sifat" id="segera" value="segera">
                            <label class="form-check-label" for="segera">Segera</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sifat" id="rahasia" value="rahasia">
                            <label class="form-check-label" for="rahasia">Rahasia</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sifat" id="penting" value="penting">
                            <label class="form-check-label" for="penting">Penting</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sifat" id="biasa" value="biasa">
                            <label class="form-check-label" for="biasa">Biasa</label>
                        </div>

                        <form style="margin-top: 20px">
                            <div class="form-group">
                                <label for="asal_surat">Asal Surat</label>
                                <input type="text" class="form-control" id="asal_surat" name="asal_surat" placeholder="Asal Surat"><?php echo $row["asal_surat"]?></input>
                            </div>

                            <div class="form-group">
                                <label for="no_surat">No Surat</label>
                                <input type="text" class="form-control" id="no_surat" name="no_surat" placeholder="No Surat">
                            </div>

                            <div class="form-group">
                                <label for="tanggal_surat">Tanggal Surat</label>
                                <input type="date" class="form-control" id="tanggal_surat" name="tanggal_surat" placeholder="Tanggal Surat">
                            </div>

                            <div class="form-group">
                                <label for="tanggal_diterima">Tanggal Diterima</label>
                                <input type="date" class="form-control" id="tanggal_diterima" name="tanggal_diterima" placeholder="Tanggal Diterima">
                            </div>

                            <div class="form-group">
                                <label for="isi_surat">Isi/Perihal</label>
                                <input type="text" class="form-control" id="isi_surat" name="isi_surat" placeholder="Isi/Perihal">
                            </div>

                            <div class="form-group">
                                <label for="lampiran">Lampiran</label>
                                <input type="text" class="form-control" id="lampiran" name="lampiran" placeholder="Lampiran">
                            </div>

                            <div class="form-group">
                                <label for="lampiran">Catatan</label>
                                <input type="text" class="form-control" id="catatan"name="catatan" placeholder="Catatan">
                            </div>
                
                            <div class="form-group">
                                <label for="inputPDF">Link PDF</label>
                                <input type="text" class="form-control" id="linkPDF" name="linkPDF" placeholder="Link PDF">
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </form>
                    </div>
                </form>    
              </div>
            </div>
          </div>

    <script>
        $(document).ready(function(){
            $('#update-surat').on('show.bs.modal', function (e) {
                var suratId = $(e.relatedTarget).attr('id')
                $.ajax({
                    url: "http://localhost/disposisi-surat/surat.php",
                    method: "POST",
                    data: { "suratId": suratId },
                })
            });
        });
    </script>
    <script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js'></script>
    <script type='text/javascript'></script>
    </body>
</html>